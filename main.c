#include <stdio.h>
#include <stdlib.h>

#include "map.h"
#include "parser.h"

t_graph g;
char *buffer;


int main(int ac, char **args)
{
    parseTextFile(args[1]);
    //parseTextFile("test.txt");
    buffer = malloc(NB_MAX_CHAR);
    do
    {
        scanf("%s", buffer);
        //printf("%s", buffer);
        if(!feof(stdin))
            treatCommand(buffer[0]);
    }
    while(!feof(stdin));
    free(buffer);
    return 0;
}

void treatCommand(char a)
{
    printf("Commad Treated !");
}
